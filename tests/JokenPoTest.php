<?php

namespace JokenPo;

class JokenPoTest extends \PHPUnit\Framework\TestCase
{
    public function testPapelXPapel()
    {
        $jokenpo = new JokenPo;
        $this->assertEquals('empate', $jokenpo->run('papel', 'papel'));
    }

    public function testRules()
    {
        $jokenpo = new JokenPo;
        $rules = '
            - Pedra empata com Pedra e ganha de Tesoura
            - Tesoura empata com Tesoura e ganha de Papel
            - Papel empata com Papel e ganha de Pedra';
        $this->assertContains($rules, $jokenpo->rules());
    }
}

