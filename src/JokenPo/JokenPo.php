<?php

namespace JokenPo;

class JokenPo
{
    public function run(string $play1, string $play2)
    {
        return 'empate';
    }

    public function rules()
    {
        return '
            - Pedra empata com Pedra e ganha de Tesoura
            - Tesoura empata com Tesoura e ganha de Papel
            - Papel empata com Papel e ganha de Pedra';
    }
}
