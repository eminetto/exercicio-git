
[![codecov](https://codecov.io/gh/Coderockr/exercicio-git/branch/master/graph/badge.svg)](https://codecov.io/gh/Coderockr/exercicio-git)

[![Build Status](https://travis-ci.org/Coderockr/exercicio-git.svg?branch=master)](https://travis-ci.org/Coderockr/exercicio-git)

[![Ebert](https://ebertapp.io/github/Coderockr/exercicio-git.svg)](https://ebertapp.io/github/Coderockr/exercicio-git)

# Descrição

Jokenpo é uma brincadeira japonesa, onde dois jogadores escolhem um dentre três possíveis itens: Pedra, Papel ou Tesoura.

O objetivo é fazer um juiz de Jokenpo que dada a jogada dos dois jogadores informa o resultado da partida.

As regras são as seguintes:

- Pedra empata com Pedra e ganha de Tesoura
- Tesoura empata com Tesoura e ganha de Papel
- Papel empata com Papel e ganha de Pedra


# Instalação

## Instalar dependências:

    php composer.phar install

## Executar os testes

    ./vendor/bin/phpunit
